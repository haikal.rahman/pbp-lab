import 'package:flutter/material.dart';
import 'package:lab_7/edit_profile_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.purple,
      ),
      home: const MyHomePage(title: 'Kuiskuy'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomePage> createState() => MyHomePageState();
}

class MyHomePageState extends State<MyHomePage> with TickerProviderStateMixin {
  bool _scrollState = false;
  late ScrollController _scrollController;
  String nama = 'Bambang Uzumaki';
  String noInduk = '1234567891';
  String status = 'Siswa';
  String email = "uzumakiB4mbang@hotmail.com";
  String pass = "sayaGanteng123";

  void setNama(String namaBaru) {
    nama = namaBaru;
  }

  void setNoInduk(String noIndukBaru) {
    noInduk = noIndukBaru;
  }

  void setStatus(String statusBaru) {
    status = statusBaru;
  }

  void setEmail(String emailBaru) {
    email = emailBaru;
  }

  void setPassword(String passwordBaru) {
    pass = passwordBaru;
  }

  @override
  void initState() {
    super.initState();
    _scrollController = ScrollController()
      ..addListener(() {
        if (_scrollController.offset >= MediaQuery.of(context).size.height) {
          _scrollState = true; //yang muncul tombol to back-to-top
        } else {
          _scrollState = false; //yang muncul tombol to back-to-bottom
        }
      });
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  void _scrollToBottom() {
    _scrollController.animateTo(_scrollController.position.maxScrollExtent,
        duration: const Duration(seconds: 1), curve: Curves.easeOut);
    setState(() {
      _scrollState = true;
    });
  }

  void _scrollToTop() {
    _scrollController.animateTo(0,
        duration: const Duration(seconds: 1), curve: Curves.easeOut);
    setState(() {
      _scrollState = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      drawer: Drawer(
        child: Material(
            child: ListView(padding: EdgeInsets.zero, children: [
          UserAccountsDrawerHeader(
            accountName: Text(nama),
            accountEmail: Text(email),
            currentAccountPicture: CircleAvatar(
                child: ClipOval(
                    child: Image.asset(
                        'assets/images/gray-transparent-default-profile.png'))),
          )
        ])),
      ),
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title:
            Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          const Text(
            'Your Profile',
            style: TextStyle(color: Colors.black),
          ),
          Image.asset(
            'assets/images/transparent-kuiskuy-logo.png',
            height: 50,
            width: 50,
          ),
        ]),
        backgroundColor: Colors.white,
        iconTheme: const IconThemeData(color: Colors.black),
      ),
      body: SingleChildScrollView(
        physics: const NeverScrollableScrollPhysics(),
        controller: _scrollController,
        child: Container(
          // Center is a layout widget. It takes a single child and positions it
          // in the middle of the parent.
          child: Column(
            // Column is also a layout widget. It takes a list of children and
            // arranges them vertically. By default, it sizes itself to fit its
            // children horizontally, and tries to be as tall as its parent.
            //
            // Invoke "debug painting" (press "p" in the console, choose the
            // "Toggle Debug Paint" action from the Flutter Inspector in Android
            // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
            // to see the wireframe for each widget.
            //
            // Column has various properties to control how it sizes itself and
            // how it positions its children. Here we use mainAxisAlignment to
            // center the children vertically; the main axis here is the vertical
            // axis because Columns are vertical (the cross axis would be
            // horizontal).
            children: <Widget>[
              Container(
                  child: Column(
                    children: [
                      Image.asset(
                        'assets/images/gray-transparent-default-profile.png',
                        height: 130,
                        width: 130,
                      ),
                      const SizedBox(height: 15),
                      Text(nama,
                          style: const TextStyle(
                            fontSize: 25,
                            color: Colors.black,
                          )),
                      const SizedBox(height: 5),
                      const Text('1234567891',
                          style: TextStyle(color: Colors.purple, fontSize: 15)),
                      const SizedBox(height: 5),
                      Text(status),
                      const SizedBox(height: 5),
                      const Text("uzumakiB4mbang@hotmail.com",
                          style: TextStyle(
                              color: Colors.blueAccent, fontSize: 15)),
                      const SizedBox(height: 15),
                      Align(
                        alignment: Alignment.center,
                        child: TextButton(
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          const EditProfilePage()));
                            },
                            child: Stack(
                              children: [
                                Container(
                                    child: const Icon(Icons.edit_outlined,
                                        color: Colors.white),
                                    decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        color: Theme.of(context)
                                            .colorScheme
                                            .secondary),
                                    padding: const EdgeInsets.all(10))
                              ],
                            )),
                      ),
                      const SizedBox(height: 15),
                      const Text('Recent Quiz'),
                      const SizedBox(height: 5),
                      Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: const [
                              Text("BST", softWrap: true),
                              Text("80/100", softWrap: true),
                              Text("SDA", softWrap: true),
                            ],
                          ),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(18),
                              gradient: const LinearGradient(
                                  begin: Alignment.centerLeft,
                                  end: Alignment.centerRight,
                                  colors: [Colors.lightBlue, Colors.purple])),
                          padding: const EdgeInsets.symmetric(
                              vertical: 5, horizontal: 5)),
                      const SizedBox(height: 10),
                      Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: const [
                              Text("RVU", softWrap: true),
                              Text("70/100", softWrap: true),
                              Text("Alin", softWrap: true),
                            ],
                          ),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(18),
                              gradient: const LinearGradient(
                                  begin: Alignment.centerLeft,
                                  end: Alignment.centerRight,
                                  colors: [Colors.lightBlue, Colors.purple])),
                          padding: const EdgeInsets.symmetric(
                              vertical: 5, horizontal: 5)),
                      const SizedBox(height: 10),
                      Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: const [
                              Text("Synchronization", softWrap: true),
                              Text("40/100", softWrap: true),
                              Text("OS", softWrap: true),
                            ],
                          ),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(18),
                              gradient: const LinearGradient(
                                  begin: Alignment.centerLeft,
                                  end: Alignment.centerRight,
                                  colors: [Colors.lightBlue, Colors.purple])),
                          padding: const EdgeInsets.symmetric(
                              vertical: 5, horizontal: 5)),
                    ],
                  ),
                  padding: const EdgeInsets.fromLTRB(10, 30, 10, 3),
                  decoration: BoxDecoration(
                      color: const Color.fromRGBO(211, 215, 248, 0.6),
                      borderRadius: BorderRadius.circular(18),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black.withOpacity(0.06),
                          spreadRadius: 5,
                          blurRadius: 7,
                          offset: const Offset(0, 3),
                        )
                      ]),
                  width: double.infinity,
                  height: 530),
              const SizedBox(height: 180),
              Container(
                  child: Column(
                    children: [
                      Image.asset(
                        'assets/images/gray-transparent-default-profile.png',
                        height: 130,
                        width: 130,
                      ),
                      const SizedBox(height: 15),
                      const Text('Bambang Uzumaki',
                          style: TextStyle(
                            fontSize: 25,
                            color: Colors.black,
                          )),
                      const SizedBox(height: 5),
                      Text(noInduk,
                          style: const TextStyle(
                              color: Colors.purple, fontSize: 15)),
                      const SizedBox(height: 5),
                      const Text('Siswa'),
                      TextButton(
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        const EditProfilePage()));
                          },
                          child: Stack(
                            children: [
                              Container(
                                  child: const Icon(Icons.edit_outlined),
                                  decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: Theme.of(context)
                                          .colorScheme
                                          .secondary))
                            ],
                          )),
                      const SizedBox(height: 30),
                      const Text('Recent Quiz'),
                      const SizedBox(height: 5),
                      Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: const [
                              Text("Nama Kuis", softWrap: true),
                              Text("80/100", softWrap: true),
                              Text("Nama Mapel", softWrap: true),
                            ],
                          ),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(18),
                              gradient: const LinearGradient(
                                  begin: Alignment.centerLeft,
                                  end: Alignment.centerRight,
                                  colors: [Colors.lightBlue, Colors.purple])),
                          padding: const EdgeInsets.symmetric(
                              vertical: 5, horizontal: 5)),
                      const SizedBox(height: 10),
                      Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: const [
                              Text("Nama Kuis", softWrap: true),
                              Text("80/100", softWrap: true),
                              Text("Nama Mapel", softWrap: true),
                            ],
                          ),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(18),
                              gradient: const LinearGradient(
                                  begin: Alignment.centerLeft,
                                  end: Alignment.centerRight,
                                  colors: [Colors.lightBlue, Colors.purple])),
                          padding: const EdgeInsets.symmetric(
                              vertical: 5, horizontal: 5)),
                      const SizedBox(height: 10),
                      Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: const [
                              Text("Nama Kuis", softWrap: true),
                              Text("80/100", softWrap: true),
                              Text("Nama Mapel", softWrap: true),
                            ],
                          ),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(18),
                              gradient: const LinearGradient(
                                  begin: Alignment.centerLeft,
                                  end: Alignment.centerRight,
                                  colors: [Colors.lightBlue, Colors.purple])),
                          padding: const EdgeInsets.symmetric(
                              vertical: 5, horizontal: 5))
                    ],
                  ),
                  padding:
                      const EdgeInsets.symmetric(vertical: 30, horizontal: 10),
                  decoration: BoxDecoration(
                      color: const Color.fromRGBO(211, 215, 248, 0.6),
                      borderRadius: BorderRadius.circular(18),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black.withOpacity(0.06),
                          spreadRadius: 5,
                          blurRadius: 7,
                          offset: const Offset(0, 3),
                        )
                      ]),
                  width: double.infinity,
                  height: 500),
              const SizedBox(height: 70),
            ],
          ),
          decoration: const BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [Colors.lightBlue, Colors.purple]),
          ),
          alignment: Alignment.center,
          padding: const EdgeInsets.symmetric(vertical: 60, horizontal: 20),
        ),
      ),
      floatingActionButton: _scrollState == false
          ? FloatingActionButton(
              heroTag: 'ScrollBtn',
              onPressed: _scrollToBottom,
              child: const Icon(Icons.arrow_downward_rounded),
              tooltip: "Lihat nilai keseluruhan anda")
          : FloatingActionButton(
              heroTag: 'ScrollBtn',
              onPressed: _scrollToTop,
              child: const Icon(Icons.arrow_upward_rounded),
              tooltip: "Kembali ke profil",
            ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}
