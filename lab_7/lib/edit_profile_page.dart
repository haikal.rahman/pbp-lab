import 'package:flutter/material.dart';
import 'package:lab_7/main.dart';

class EditProfilePage extends StatefulWidget {
  const EditProfilePage({Key? key}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  @override
  State<EditProfilePage> createState() => _EditProfilePageState();
}

class _EditProfilePageState extends State<EditProfilePage>
    with TickerProviderStateMixin {
  final _namaKey = GlobalKey<FormState>();
  final _noIndukKey = GlobalKey<FormState>();
  final _emailKey = GlobalKey<FormState>();
  final _passKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      drawer: Drawer(
        child: Material(
            child: ListView(padding: EdgeInsets.zero, children: [
          UserAccountsDrawerHeader(
            accountName: const Text('Dummy name'),
            accountEmail: const Text('dummy123@gmail.com'),
            currentAccountPicture: CircleAvatar(
                child: ClipOval(
                    child: Image.asset(
                        'assets/images/gray-transparent-default-profile.png'))),
          )
        ])),
      ),
      appBar: AppBar(
        // Here we take the value from the EditProfilePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title:
            Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          const Text(
            'Your Profile',
            style: TextStyle(color: Colors.black),
          ),
          Image.asset(
            'assets/images/transparent-kuiskuy-logo.png',
            height: 50,
            width: 50,
          ),
        ]),
        backgroundColor: Colors.white,
        iconTheme: const IconThemeData(color: Colors.black),
      ),
      body: SingleChildScrollView(
        physics: const NeverScrollableScrollPhysics(),
        child: Container(
          // Center is a layout widget. It takes a single child and positions it
          // in the middle of the parent.
          child: Column(
            // Column is also a layout widget. It takes a list of children and
            // arranges them vertically. By default, it sizes itself to fit its
            // children horizontally, and tries to be as tall as its parent.
            //
            // Invoke "debug painting" (press "p" in the console, choose the
            // "Toggle Debug Paint" action from the Flutter Inspector in Android
            // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
            // to see the wireframe for each widget.
            //
            // Column has various properties to control how it sizes itself and
            // how it positions its children. Here we use mainAxisAlignment to
            // center the children vertically; the main axis here is the vertical
            // axis because Columns are vertical (the cross axis would be
            // horizontal).
            children: <Widget>[
              Container(
                  child: Column(
                    children: [
                      SizedBox(
                          child: Stack(fit: StackFit.loose, children: [
                            Image.asset(
                              'assets/images/gray-transparent-default-profile.png',
                              height: 130,
                              width: 130,
                            ),
                            Align(
                                alignment: Alignment.bottomRight,
                                child: TextButton(
                                    onPressed: () {
                                      showDialog(
                                          context: context,
                                          builder: (context) {
                                            return Dialog(
                                                child: Container(
                                              child: const Text(
                                                  "Maaf, fitur belum diimplementasi",
                                                  textAlign: TextAlign.center),
                                              padding: const EdgeInsets.all(10),
                                            ));
                                          });
                                    },
                                    child: Stack(
                                      children: [
                                        Container(
                                            child: const Icon(
                                                Icons.edit_outlined,
                                                color: Colors.white),
                                            decoration: BoxDecoration(
                                                shape: BoxShape.circle,
                                                color: Theme.of(context)
                                                    .colorScheme
                                                    .secondary),
                                            padding: const EdgeInsets.all(5))
                                      ],
                                    )))
                          ]),
                          height: 140,
                          width: 140),
                      const SizedBox(height: 15),
                      TextFormField(
                          key: _namaKey,
                          initialValue: MyHomePageState().nama,
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return "Harap tulis nama baru anda";
                            } else {
                              MyHomePageState().setNama(value);
                            }
                          },
                          textAlign: TextAlign.center,
                          style: const TextStyle(
                              fontSize: 25, color: Colors.white)),
                      TextFormField(
                          key: _noIndukKey,
                          initialValue: MyHomePageState().noInduk,
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return "Harap tulis nomor induk baru anda";
                            } else {
                              MyHomePageState().setNoInduk(value);
                            }
                          },
                          textAlign: TextAlign.center,
                          style: const TextStyle(
                              fontSize: 15, color: Colors.white)),
                      TextFormField(
                          key: _emailKey,
                          initialValue: MyHomePageState().email,
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return "Harap tulis email baru anda";
                            } else {
                              MyHomePageState().setEmail(value);
                            }
                          },
                          textAlign: TextAlign.center,
                          style: const TextStyle(
                              fontSize: 15, color: Colors.white)),
                      TextFormField(
                          key: _passKey,
                          initialValue: MyHomePageState().pass,
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return "Harap tulis password baru anda";
                            } else {
                              MyHomePageState().setPassword(value);
                            }
                          },
                          textAlign: TextAlign.center,
                          style: const TextStyle(
                              fontSize: 15, color: Colors.white)),
                      const SizedBox(height: 25),
                      TextButton(
                          onPressed: () {
                            // if (
                            //   _namaKey.currentState!.validate() &&
                            //   _noIndukKey.currentState!.validate() &&
                            //   _emailKey.currentState!.validate() &&
                            //   _passKey.currentState!.validate()
                            //   ){
                            //   MyHomePageState().setState(() {});
                            // }
                            Navigator.pop(context);
                          },
                          child: Stack(
                            children: [
                              Container(
                                  child: const Icon(Icons.check_rounded,
                                      color: Colors.white),
                                  decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: Theme.of(context)
                                          .colorScheme
                                          .secondary),
                                  padding: const EdgeInsets.all(10))
                            ],
                          )),
                    ],
                  ),
                  padding:
                      const EdgeInsets.symmetric(vertical: 30, horizontal: 10),
                  decoration: BoxDecoration(
                      color: const Color.fromRGBO(211, 215, 248, 0.6),
                      borderRadius: BorderRadius.circular(18),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black.withOpacity(0.06),
                          spreadRadius: 5,
                          blurRadius: 7,
                          offset: const Offset(0, 3),
                        )
                      ]),
                  width: double.infinity,
                  height: 500),
              const SizedBox(height: 70)
            ],
          ),
          decoration: const BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [Colors.lightBlue, Colors.purple]),
          ),
          alignment: Alignment.center,
          padding: const EdgeInsets.symmetric(vertical: 60, horizontal: 20),
        ),
      ),
    );
  }
}
