1. Apakah perbedaan antara JSON dan XML?
- Perbedaan pertama adalah setiap objek pada JSON direpresentasikan sebagai multi-dimensional dictionary, smenetara XML direpresentasikan sebagai element tree.
- Selanjutnya, JSON berdasarkan bahasa JavaScript, sementara XML merupakan markup language. Sehingga JSON bisa di-parse dengan fungsi JavaScript biasa, sementara XML menggunakan Parser yang berbeda bergantung kebutuhan.
- JSON tidak memerlukan end tag, hanya pasangan key-value saja. Sementara XML memerlukan tag dan beberapa informasi tambahan.
- JSON lebih mudah dibaca, serta lebih cepat di-parse karena struktur datanya yang berupa map (dibanding XML yang berupa tree).
- JSON merepresentasikan data, sementara XML merepresentasikan informasi secara jelas.
- XML mengsupport namespaces, comments, dan metadata sementara JSON tidak.


2. Apakah perbedaan antara HTML dan XML?
- HTML tidak case sensitive, sementara XML case sensitive.
- Memiliki tags yang pre-defined (disediakan), sementara XML memeliki user-defined tags.
- Digunakan untuk menampilan data, sementara XML digunakan untuk meng-transfer data dari dan menuju database.
- Beberapa tag HTML tidak membutuhkan closing tags, sementara semua tag pada XML memerlukan closing tag.
- HTML bersifat static, sedangkan XML bersifat dynamic.
