from django.urls import path, re_path
from .views import friend_list, index

urlpatterns = [
    path('', index, name='index'),
    # TODO Add friends path using friend_list Views
    re_path('friends', friend_list, name='friend_list')
]
