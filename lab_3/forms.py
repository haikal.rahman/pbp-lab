from django import forms
from lab_1.models import Friend

class FriendForm(forms.ModelForm):

    class Meta:
        model = Friend
        fields = [
            "name",
            "npm",
            "date_of_birth"
        ]
        
    error_messages = {"required": "Please Type"}
    attrs = { 
        "name": {
            "type": "text",
            "placeholder": "Isi nama kamu di sini"
        },
        "npm": {
            "type": "text",
            "placeholder": "Isi NPM kamu di sini"
        },
        "date_of_birth": {
            "type": "text",
            "placeholder": "yyyy-mm-dd"
        }
    }

    name = forms.CharField(
        label = "", 
        required = False,
        max_length = 30,
        widget = forms.TextInput(attrs = attrs["name"])
    )
    npm = forms.IntegerField(
        label = "",
        required = False,
        widget = forms.NumberInput(attrs = attrs["npm"])
    )
    date_of_birth = forms.DateField(
        label = "",
        required = False,
        widget = forms.DateInput(attrs = attrs["date_of_birth"])
    )