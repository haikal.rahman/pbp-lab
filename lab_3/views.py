from django.http import response
from django.shortcuts import render
from django.http.response import HttpResponseRedirect
from .forms import FriendForm
from .models import Friend
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required(login_url = "/admin/login/")
def index(request):
    friends = Friend.objects.all().values() 
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

@login_required(login_url = "/admin/login/")
def add_friend(request):
    form = FriendForm(request.POST or None) 
    formm = {"form": form}

    if (form.is_valid() and request.method == "POST"):
        form.save()
        return HttpResponseRedirect('/lab-3')
    else:
        return render(request, 'lab3_forms.html', formm)
