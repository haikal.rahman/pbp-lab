# Generated by Django 3.2.7 on 2021-09-27 07:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lab_2', '0002_auto_20210927_1358'),
    ]

    operations = [
        migrations.AlterField(
            model_name='note',
            name='message',
            field=models.CharField(max_length=1000),
        ),
    ]
