from django.http import response
from django.shortcuts import render
from .models import Note
from django.http.response import HttpResponse
from django.core import serializers


def index(request):
    notes = Note.objects.all().values()
    response = {'notes': notes}
    return render(request, 'lab2.html', response)

def xml(request):
    notes = Note.objects.all()
    data = serializers.serialize('xml', notes)
    return HttpResponse(data, content_type = "application/xml") 

def json(request):
    notes = Note.objects.all()
    data = serializers.serialize('json', notes)
    return HttpResponse(data, content_type = "application/json")