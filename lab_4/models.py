from django.db import models

# Create your models here.
class Note(models.Model):
    addresser = models.CharField(max_length = 50, verbose_name="addresser")
    addressee = models.CharField(max_length = 50)
    title = models.CharField(max_length = 50)
    message = models.TextField()
