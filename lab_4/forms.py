from django import forms
from lab_4.models import Note

class NoteForm(forms.ModelForm):

    class Meta:
        model = Note
        fields = [
            "addressee",
            "addresser",
            "title",
            "message"
        ]
        
    error_messages = {
        "required": "Please Type",
    }
    attrs = { 
        "addressee": {
            "type": "text",
            "placeholder": "John Doe"
        },
        "addresser": {
            "type": "text",
            "placeholder": "Alice Bob"
        },
        "title": {
            "type": "text",
            "placeholder": "The Great Titan War"
        },
        "message": {
            "type": "text",
            "placeholder": "yes"
        }
    }

    addresser = forms.CharField(
        label = "", 
        max_length = 30,
        widget = forms.TextInput(attrs = attrs["addresser"])
    )
    addressee = forms.CharField(
        label = "",
        max_length = 30,
        widget = forms.TextInput(attrs = attrs["addressee"])
    )
    title = forms.CharField(
        label = "",
        max_length = 50,
        widget = forms.TextInput(attrs = attrs["title"])
    )
    message = forms.CharField(
        label = "",
        widget = forms.Textarea(attrs = attrs["message"])
    )